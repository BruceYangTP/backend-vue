import axios from "axios";
// import { useStore } from "vuex";
import { ElMessageBox } from "element-plus";

// const store = useStore();
const instance = axios.create({
  baseURL: process.env.VUE_APP_AXIOS_BASE_URL_DEV,
  headers: {
    "Access-Control-Allow-Credentials": true,
    "Access-Control-Allow-Headers":
      "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods":
      "GET, PUT, POST, DELETE, OPTIONS, HEAD, PATCH",
    "Access-Control-Max-Age": "86400",
    "Access-Control-Expose-Headers": "Content-Security-Policy, Location",
    "Cache-Control": "no-cache",
  },
});

// instance為request create的實體
instance.interceptors.request.use(
  function (config) {
    config.headers = {
      Authorization: `Bearer ${sessionStorage.getItem("token") || ""}`,
    };
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// instance為in load create的實體
instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          ElMessageBox.alert(`${error.response.data.message}`, "錯誤", {
            confirmButtonText: "確定",
            type: "warning",
          });
          break;
        case 404:
          ElMessageBox.alert(`${error.response.data.message}`, "錯誤", {
            confirmButtonText: "確定",
            type: "warning",
          });
          break;
        case 422:
          ElMessageBox.alert(`${error.response.data.message}`, "錯誤", {
            confirmButtonText: "確定",
            type: "warning",
          });
          break;
        case 500:
          ElMessageBox.alert(`${error.response.data.message}`, "錯誤", {
            confirmButtonText: "確定",
            type: "warning",
          });
          break;

        default:
          console.log(error.message);
      }
    }
    if (!window.navigator.onLine) {
      alert("網路出了點問題，請重新連線後重整網頁");
      return;
    }
    return Promise.reject(error);
  }
);

export default function (method, url, data, config) {
  method = method.toLowerCase();
  switch (method) {
    case "post":
      return instance.post(url, data, config);
    case "get":
      return instance.get(url, { params: data });
    case "delete":
      return instance.delete(url, { params: data });
    case "put":
      return instance.put(url, data);
    case "patch":
      return instance.patch(url, data);
    default:
      console.log(`未知的 method: ${method}`);
      return false;
  }
}
