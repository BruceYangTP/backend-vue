import req from "./Api";

/**
 * 登入
 * @param {username,password} registered
 * @returns
 */
export const userLogin = (registered) => {
  return req("post", "user/login", registered);
};

/**
 * 登出
 * @returns
 */
export const userLogout = () => {
  return req("post", "/user/logout");
};

/**
 * 新增使用者
 * @param {username,password,confirm_password,name,email,organization,status} req
 * @returns
 */
export const addUser = (request) => {
  return req("post", "/user", request);
};

/**
 * 使用者列表
 * @param {}
 * @returns
 */
export const userList = () => {
  return req("get", "/user");
};

/**
 * 檢視使用者
 * @param {}
 * @returns
 */
export const getUser = (id) => {
  return req("get", `/user/${id}`);
};

/**
 * 刪除使用者
 * @param {}
 * @returns
 */
export const deleteUser = (id) => {
  return req("delete", `/user/${id}`);
};

/**
 * 更新使用者
 * @param {}
 * @returns
 */
export const putUser = (id, req) => {
  return req("put", `/user/${id}`, req);
};
