import { createRouter, createWebHistory } from "vue-router";
import Home from "@/views/Home.vue";
import Dashboard from "@/views/Dashboard.vue";
import NotFoundComponent from "../views/NotFoundComponent";
import MainIndex from "@/views/MainIndex";
import CustomerList from "@/views/Customer/CustomerList";
import CustomerAdd from "@/views/Customer/CustomerAdd";
import OrganizationList from "@/views/Organization/OrganizationList";
import OrganizationAdd from "@/views/Organization/OrganizationAdd";
import ProductList from "@/views/Product/ProductList";
import ProductAdd from "@/views/Product/ProductAdd";
import WarehouseList from "@/views/Warehouse/WarehouseList";
import WarehouseAdd from "@/views/Warehouse/WarehouseAdd";
import PurchaseList from "@/views/Purchase/PurchaseList";
import PurchaseAdd from "@/views/Purchase/PurchaseAdd";
import SalesList from "@/views/Sales/SalesList";
import SalesAdd from "@/views/Sales/SalesAdd";
import ReservationList from "@/views/Reservation/ReservationList";
import ReservationAdd from "@/views/Reservation/ReservationAdd";
import RosterList from "@/views/Roster/RosterList";
import RosterAdd from "@/views/Roster/RosterAdd";
import ManagementList from "@/views/Management/ManagementList";
import ManagementAdd from "@/views/Management/ManagementAdd";
import ManagementAdd_id from "@/views/Management/ManagementAdd_id";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/dashboard",
    component: Dashboard,
    name: "Dashboard",
    children: [
      // Main index
      {
        path: "main-index",
        name: "MainIndex",
        component: MainIndex,
      },
      // Customer
      {
        path: "customer-list",
        name: "CustomerList",
        component: CustomerList,
      },
      {
        path: "customer-add",
        name: "CustomerAdd",
        component: CustomerAdd,
      },
      // 進銷存系統-組織
      {
        path: "organization-list",
        name: "OrganizationList",
        component: OrganizationList,
      },
      {
        path: "organization-add",
        name: "OrganizationAdd",
        component: OrganizationAdd,
      },
      // 進銷存系統-產品
      {
        path: "product-list",
        name: "ProductList",
        component: ProductList,
      },
      {
        path: "product-add",
        name: "ProductAdd",
        component: ProductAdd,
      },
      // 進銷存系統-倉庫
      {
        path: "warehouse-list",
        name: "WarehouseList",
        component: WarehouseList,
      },
      {
        path: "warehouse-add",
        name: "WarehouseAdd",
        component: WarehouseAdd,
      },
      // 進銷存系統-進貨
      {
        path: "purchase-list",
        name: "PurchaseList",
        component: PurchaseList,
      },
      {
        path: "purchase-add",
        name: "PurchaseAdd",
        component: PurchaseAdd,
      },
      // 進銷存系統-銷售
      {
        path: "sales-list",
        name: "SalesList",
        component: SalesList,
      },
      {
        path: "sales-add",
        name: "SalesAdd",
        component: SalesAdd,
      },
      // 預約系統-預約
      {
        path: "reservation-list",
        name: "ReservationList",
        component: ReservationList,
      },
      {
        path: "reservation-add",
        name: "ReservationAdd",
        component: ReservationAdd,
      },
      // 人事管理系統-排班
      {
        path: "roster-list",
        name: "RosterList",
        component: RosterList,
      },
      {
        path: "roster-add",
        name: "RosterAdd",
        component: RosterAdd,
      },
      // 人事管理系統-User Management
      {
        path: "management-list",
        name: "ManagementList",
        component: ManagementList,
      },
      {
        path: "management-add/",
        name: "ManagementAdd",
        component: ManagementAdd,
      },
      {
        path: "management-add/:id",
        name: "ManagementAdd_id",
        component: ManagementAdd_id,
      },
    ],
  },
  {
    path: "/:catchAll(.*)",
    component: NotFoundComponent,
    name: "NotFound",
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
