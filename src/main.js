import { createApp } from "vue";
import router from "./router";
import App from "./App.vue";
import store from "./store";
// 如果要使用.scss样式文件，则需要引入base.scss文件
import "element-plus/packages/theme-chalk/src/base.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCalendarAlt,
  faCircle,
  faAlignJustify,
  faSignOutAlt,
  faBuilding,
  faBoxes,
  faWarehouse,
  faTruckLoading,
  faChartBar,
  faUsers,
  faEdit,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(
  faCalendarAlt,
  faCircle,
  faAlignJustify,
  faSignOutAlt,
  faBuilding,
  faBoxes,
  faWarehouse,
  faTruckLoading,
  faChartBar,
  faUsers,
  faEdit,
  faTrashAlt
);

import {
  ElButton,
  ElSelect,
  ElCard,
  ElInput,
  ElMenu,
  ElMenuItemGroup,
  ElSubmenu,
  ElMenuItem,
  ElRadioGroup,
  ElRadioButton,
  ElRow,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElTable,
  ElTableColumn,
  ElOption,
  ElMessageBox,
  ElCheckbox,
  ElCheckboxGroup,
} from "element-plus";

const app = createApp(App).component("font-awesome-icon", FontAwesomeIcon);
app.use(ElButton);
app.use(ElSelect);
app.use(ElCard);
app.use(ElInput);
app.use(ElMenu);
app.use(ElMenuItemGroup);
app.use(ElSubmenu);
app.use(ElMenuItem);
app.use(ElRadioGroup);
app.use(ElRadioButton);
app.use(ElRow);
app.use(ElBreadcrumb);
app.use(ElBreadcrumbItem);
app.use(ElTable);
app.use(ElTableColumn);
app.use(ElOption);
app.use(ElMessageBox);
app.use(ElCheckbox);
app.use(ElCheckboxGroup);

require("dotenv").config();
app.use(store);
app.use(router);
app.mount("#app");
