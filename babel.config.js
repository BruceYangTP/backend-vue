module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: [
    [
      "import",
      {
        libraryName: "element-plus",
        customStyleName: (name) => {
          name = name.slice(3);
          return `element-plus/packages/theme-chalk/src/${name}.scss`;
        },
      },
    ],
  ],
  // devServer: {
  //   proxy: "http://localhost/api/v1",
  // },
  // publicPath:
  //   process.env.NODE_ENV === "production" ? "http://localhost/api/v1" : "/",
};
